package ui.icons;

import com.intellij.ui.JBColor;

import java.awt.*;

public class CaseineColors {
    public final static JBColor NONE = new JBColor(new Color(0, 0, 0, 0), new Color(0, 0, 0, 0));
    public final static JBColor OUTSTANDING_GRAY = new JBColor(new Color(89, 89, 89), new Color(175, 177, 179));
    public final static JBColor DISCRETE_GRAY = new JBColor(new Color(175, 177, 179), new Color(89, 89, 89));
    public final static JBColor GREEN = new JBColor(new Color(73, 156, 84), new Color(73, 156, 84));
    public final static JBColor BLUE = new JBColor(new Color(62, 134, 160), new Color(62, 134, 160));
    public final static JBColor RED = new JBColor(new Color(199, 84, 80), new Color(199, 84, 80));
}
