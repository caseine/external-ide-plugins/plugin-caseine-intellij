/*
 * Copyright (C) 2022 Joshua Monteiller, Astor Bizard, Christophe Saint-Marcel
 *
 * Caseine Plugin for IntelliJ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Caseine Plugin for IntelliJ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ui.icons;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

/**
 * This class stocks all the icons needed for the plugin
 *
 * @author Joshua Monteiller
 */
public class CaseineIcons {

    public static final Icon Milk = IconLoader.getIcon("/ui/icons/lait_caseine.png", CaseineIcons.class);
    public static final Icon NewCaseine = IconLoader.getIcon("/ui/icons/new_project_caseine.png", CaseineIcons.class);
    public static final Icon Caseine = IconLoader.getIcon("/ui/icons/caseine.png", CaseineIcons.class);
    public static final Icon Pull = IconLoader.getIcon("/ui/icons/caseine_arrow_pull.png", CaseineIcons.class);
    public static final Icon Push = IconLoader.getIcon("/ui/icons/caseine_arrow_push.png", CaseineIcons.class);
    public static final Icon Reset = IconLoader.getIcon("/ui/icons/reset.png", CaseineIcons.class);
    public static final Icon Evaluate = IconLoader.getIcon("/ui/icons/evaluate.png", CaseineIcons.class);

    public static final Icon CaseError = IconLoader.getIcon("/ui/icons/JUnit_CaseError.png", CaseineIcons.class);
    public static final Icon CaseFail = IconLoader.getIcon("/ui/icons/JUnit_CaseFail.png", CaseineIcons.class);
    public static final Icon CaseOk = IconLoader.getIcon("/ui/icons/JUnit_CaseOk.png", CaseineIcons.class);
    public static final Icon SuiteError = IconLoader.getIcon("/ui/icons/JUnit_SuiteError.png", CaseineIcons.class);
    public static final Icon SuiteFail = IconLoader.getIcon("/ui/icons/JUnit_SuiteFail.png", CaseineIcons.class);
    public static final Icon SuiteOk = IconLoader.getIcon("/ui/icons/JUnit_SuiteOk.png", CaseineIcons.class);
    public static final Icon RedTimer = IconLoader.getIcon("/ui/icons/red_timer.svg", CaseineIcons.class);
    public static final Icon GreenTimer = IconLoader.getIcon("/ui/icons/green_timer.svg", CaseineIcons.class);
    public static final Icon OrangeTimer = IconLoader.getIcon("/ui/icons/orange_timer.svg", CaseineIcons.class);
    public static final Icon Eval = IconLoader.getIcon("/ui/icons/eval.svg", CaseineIcons.class);
}
