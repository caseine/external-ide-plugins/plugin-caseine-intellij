/*
 * Copyright (C) 2022 Joshua Monteiller, Astor Bizard, Christophe Saint-Marcel
 *
 * Caseine Plugin for IntelliJ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Caseine Plugin for IntelliJ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package action;

import com.intellij.notification.Notification;
import com.intellij.notification.NotificationAction;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.options.ShowSettingsUtil;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import module.CaseineModuleBuilder;
import notifications.CaseineNotifier;
import org.jetbrains.annotations.NotNull;
import service.PersistentStorage;
import service.ServiceGetter;
import settings.SettingsConfigurable;
import sideWindow.CaseineSideWindow;
import sideWindow.CaseineSideWindowFactory;
import ui.icons.CaseineIcons;
import vplwsclient.FileUtils;
import vplwsclient.RestJsonMoodleClient;
import vplwsclient.VplFile;
import vplwsclient.exception.*;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.swing.tree.DefaultMutableTreeNode;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.NoSuchFileException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static module.CaseineModuleBuilder.createVplignoreFile;
import static service.ServiceGetter.retrieveFiles;

/**
 * Exception used to exit control flow but not supposed to be handled (usually thrown after handling an exception prematurely).
 */
class AlreadyTreatedException extends Exception {}

/**
 * This class served to add actions to different buttons who appeared on the top of the Caseine side window.
 * The position of these buttons are defined in the CaseineActionGroup class.
 *
 * @author Joshua Monteiller
 */
public class CaseineAction extends AnAction {

    private ServiceGetter servGet;

    private Project currentProject;

    private String basePath;

    /**
     * Perform an action when any button is used.
     * This method is called when push, pull, reset or evaluate action
     * is triggered by the user.
     *
     * @param event AnActionEvent which served to retrieve the project
     */
    @Override
    public void actionPerformed(@NotNull AnActionEvent event) {
        currentProject = event.getProject(); //Retrieve the project from where the button was pressed
        String command = event.getPresentation().getText(); //The command represents the pressed button
        basePath = currentProject.getBasePath() + File.separator; //The path of where to found the project and the files associated with

        //If the ServiceGetter is not initialized, initialize it or if the path to the special file have changed, recreate the ServiceGetter
        if (servGet == null || servGet.isPathModified(basePath) || command.equals("Reset")) {
            servGet = new ServiceGetter(basePath);
        } else {
            servGet.updateModified(); // Make sure everything is correct and has not been changed
        }

        FileDocumentManager.getInstance().saveAllDocuments();

        if (servGet.haveFailed()) {
            //If the connection have failed, show an error message to the user
            CaseineNotifier.notifyConnectionError(currentProject, true);
            return;
        }

        // Successful Connection
        try {
            switch (command) {
                case "Reset":
                    if (pull(true)) {
                        CaseineNotifier.notifyInfo(currentProject, "Caseine Lab successfully reset", "", CaseineIcons.Reset); // Notification should depend on the context instead of the action performed so that it can be called from different context.
                    }
                    break;
                case "Pull":
                    if (pull(false)) {
                        CaseineNotifier.notifyInfo(currentProject, "Caseine Lab successfully imported", "", CaseineIcons.Pull); // Notification should depend on the context instead of the action performed so that it can be called from different context.
                    }
                    break;
                case "Push":
                    push();
                    CaseineNotifier.notifyInfo(currentProject, "Caseine Lab successfully exported", "", CaseineIcons.Push); // Notification should depend on the context instead of the action performed so that it can be called from different context.
                    break;
                case "Evaluate":
                    // Notify that the evaluation has started.
                    CaseineNotifier.notifyInfo(currentProject, "Evaluation is in progress...", "", CaseineIcons.Evaluate); // Notification should depend on the context instead of the action performed so that it can be called from different context.
                    // Evaluation is a long action, it will run as a separate Thread that will inform the user about its progress.
                    launchEvaluation();
                    break;
                default:
                    CaseineNotifier.notifyError(currentProject, "ERROR : Internal code error", "Selected button doesn't exist or its name is misspelled in the code");
            }
        } catch (IOException | VplException e) {
            handleActionException(e, command);
        }
    }

    /**
     * Handle the exceptions caused by caseine actions (except new project).
     * !! Make sure that this function is called at the end of the action handling !!
     * Otherwise you may end up notifying the user several time for the same problem.
     *
     * @param e       Raised exception to handle. It can be of type {@link NoSuchFileException}, {@link MaxFilesException}, {@link IOException}, {@link VplException}, {@link InterruptedException} or {@link AlreadyTreatedException}.
     * @param command A string indicating the handled action that failed.
     */
    private void handleActionException(Exception e, String command) {
        if (e instanceof RequiredFileNotFoundException) {
            CaseineNotifier.notifyError(currentProject, "Required Files are missing.", e.getMessage());
            return;
        }
        if (e instanceof NoSuchFileException) {
            CaseineNotifier.notifyError(currentProject, "ERROR : file not found", "The files you want to push or evaluate are not in the project/have been deleted");
            return;
        }
        if (e instanceof MaxFilesException) {
            CaseineNotifier.notifyError(currentProject, "Maximum Number Of Files Exceeded", e.getMessage(),
                    new NotificationAction[]{
                            new NotificationAction("Open .vplignore") {
                                @Override
                                public void actionPerformed(@NotNull AnActionEvent e, @NotNull Notification notification) {
                                    File vplignoreFile = new File(currentProject.getBasePath() + File.separator + ".vplignore");
                                    if (!vplignoreFile.exists())
                                        createVplignoreFile(currentProject);
                                    final FileEditorManager fileEditorManager = FileEditorManager.getInstance(currentProject);
                                    VirtualFile vFile = LocalFileSystem.getInstance().findFileByPath(vplignoreFile.getPath());
                                    if (vFile != null) {
                                        fileEditorManager.openFile(vFile, true);
                                    }
                                }
                            }
                    });
            return;
        }
        if (e instanceof IOException || e instanceof VplException || e instanceof InterruptedException) {
            CaseineNotifier.notifyError(currentProject, "Error During Caseine Action : " + command, "IOException, VplException or InterruptedException occurred\n" + e.getMessage());
            return;
        }
        if (e instanceof AlreadyTreatedException) {
            // The problem has already been dealt with. Just do nothing.
            return;
        }
        // If this line is reach, there is an error in the plugin implementation.
        throw new RuntimeException("Internal error. Please report this at https://gricad-gitlab.univ-grenoble-alpes.fr/caseine/external-ide-plugins/plugin-caseine-intellij/-/issues", e);
    }

    /**
     * Pull current project from the vpl.
     *
     * @param reset {@code true} If this method is called by a reset action and project should be pull from initial project state. Else, the project will be pulled from the last saved (pushed/evaluated) version of your project.
     * @return {@code true} if the pull was performed, {@code false} if it was cancelled by the user.
     * @throws MoodleWebServiceException In case an error happened in the moodle client.
     * @throws VplConnectionException    In case of a connexion issue.
     * @throws IOException               In case of an error with the local file system.
     */
    private boolean pull(boolean reset) throws MoodleWebServiceException, VplConnectionException, IOException {
        String warningMsg = "Warning! You are about to overwrite your project for the exercise: " + servGet.getExerciseName() + ".\nAre you sure you want to continue ?";
        int ok_cancel;
        //Ask the user if he really wants to erase the actual file
        ok_cancel = Messages.showOkCancelDialog(currentProject, warningMsg, "Confirmation", "OK", "Cancel", Messages.getQuestionIcon());

        if (ok_cancel == Messages.OK) {
            //Get the JsonArray which represent the required/initial files
            JsonArray JArray;
            if (reset) {
                JArray = servGet.getReqFiles();
            } else {
                JArray = servGet.getFiles();
            }
            // In case of a reset, create .vplignore file
            if (reset) {
                new File(currentProject.getBasePath() + File.separator + ".vplignore").delete(); // Delete vplignore to assure it refresh
                CaseineModuleBuilder.createVplignoreFile(currentProject);
            }
            //Proceed to reset the files
            retrieveFiles(JArray, basePath);
            return true;
        } else {
            return false;
        }
    }

    /**
     * If needed, update the IML file to allow execution of local java programs.
     *
     * @param e Event received when the associated group-id menu is chosen.
     */
    @Override
    public void update(@NotNull AnActionEvent e) {
        e.getPresentation().setEnabled(true);
        Project project = e.getProject();

        //Initialize the listeners
        if (project != null) {

            //If a new project is created
            PersistentStorage state = PersistentStorage.getInstance();
            if (state.newProjectStatus) {
                //Create the path to the file ".iml" which is always in the root directory of the project with the project name in it
                String filePath = project.getBasePath() + "/" + project.getName() + ".iml";

                //If it exists, change it. The change will be crucial for IntelliJ to allow to execute java programs locally
                File imlFile = new File(filePath);
                if (imlFile.exists()) {
                    boolean success = changeIML(imlFile);
                    if (success) {
                        //If the file was changed, refresh it later
                        VirtualFile virtualFile = LocalFileSystem.getInstance().findFileByIoFile(new File(filePath));
                        if (virtualFile != null) {
                            ApplicationManager.getApplication().invokeLater(() -> virtualFile.refresh(false, false));
                        }
                        //Change back the boolean to false
                        state.newProjectStatus = false;
                    }
                }
            }
        }
    }


    /**
     * Call the client to push all the files of the project to save them on the platform.
     *
     * @throws VplException En error occurred within the interaction with the web service
     * @throws IOException  An error occurred within the interaction with local file system
     */
    public void push() throws VplException, IOException {
        List<VplFile> listVFile = new ArrayList<>();
        String path = currentProject.getBasePath();
        List<String> excludedFiles = FileUtils.scanExcludedList(path);
        //Browse all the files and directory of the project
        List<File> listFiles = FileUtils.listIncludedFiles(path, path, excludedFiles);
        for (File file : listFiles) {
            String standardFilePath = file.getPath()
                    .replace(File.separator, "/");
            VplFile VFile = new VplFile(file, standardFilePath.replace(path + "/", ""));
            listVFile.add(VFile);
        }
        //Call the service to save all the file in the list given in arguments
        RestJsonMoodleClient RJMC = servGet.getRJMC();
        RJMC.callServiceWithFiles("mod_vpl_save", listVFile);
    }

    /**
     * Call the client to push all the files needed for the exercise, evaluate them and return the results to the user.
     * This action takes quite some time so it is ran in a separate thread and warn the user about itself and handle its own exceptions.
     */
    public void launchEvaluation() {
        new Thread(() -> {
            CaseineSideWindow csw = CaseineSideWindowFactory.getCsw(basePath);
            try {
                // Avert the user that the evaluation is running
                csw.setSelectedTab();
                csw.setResultsTree("", "");
                csw.setResultGrade("Evaluation is running...", true);

                //We need to push the file before the evaluation
                push();

                PersistentStorage state = PersistentStorage.getInstance();
                RestJsonMoodleClient RJMC = servGet.getRJMC();
                JsonObject jsonFile = waitEvaluationResult(RJMC, state); // Get the results from server once computed.

                //Set the grade and the result tree in the side window
                csw.setSelectedTab();
                csw.setResultsTree(jsonFile.getString("evaluation"), (new SimpleDateFormat()).format(new Date()));
                csw.setResultGrade(jsonFile.getString("grade"));
                ApplicationManager.getApplication().invokeLater(() -> csw.setNodeExpanded(csw.resultsTree, (DefaultMutableTreeNode) csw.resultsTree.getModel().getRoot()));

                CaseineNotifier.notifyInfo(currentProject, "Evaluation finished", "", CaseineIcons.Evaluate);
            } catch (InterruptedException | AlreadyTreatedException | VplException | IOException e) {
                handleActionException(e, "Evaluate");
                csw.initResults(currentProject.getBasePath() + "/");
            }
        }).start();
    }

    /**
     * Wait the results of the evaluation.
     *
     * @param RJMC  Moodle client used to create websocket (web socket is created in this method).
     * @param state The persistent storage link to the project.
     * @return A JsonObject with the evaluation results.
     * @throws VplConnectionException    En error occurred within the interaction with the web service
     * @throws MoodleWebServiceException In case an error happened in the moodle client.
     * @throws InterruptedException      Another Thread interrupted the wait and query process.
     * @throws AlreadyTreatedException   An exception was thrown and execution flow should not be continued. Yet nothing else should be done about this exception as it has already been treated.
     */
    @NotNull
    private JsonObject waitEvaluationResult(RestJsonMoodleClient RJMC, PersistentStorage state) throws VplConnectionException, MoodleWebServiceException, InterruptedException, AlreadyTreatedException {
        JsonObject jsonFile; //The JSON file
        if (state.recommendedRadioButtonSelected) { // Recommended (websocket) mode.
            jsonFile = webSocketWait(RJMC);
        } else { // Compatibility mode.
            jsonFile = compatibilityWait(RJMC);
        }
        return jsonFile;
    }

    /**
     * Wait for evaluation result with repeatedly calling webservice.
     *
     * @param RJMC Moodle client used to create websocket (web socket is created in this method).
     * @return A {@code JsonObject} containing the results of the evaluation.
     * @throws VplConnectionException    En error occurred within the interaction with the web service
     * @throws MoodleWebServiceException In case an error happened in the moodle client.
     * @throws InterruptedException      Another Thread interrupted the wait and query process.
     */
    @NotNull
    private JsonObject compatibilityWait(RestJsonMoodleClient RJMC) throws VplConnectionException, MoodleWebServiceException, InterruptedException {
        JsonObject jsonFile = null;
        final Object lock = new Object(); //The lock needed to wait
        int waitTime = 2000; //The number of milliseconds needed to wait
        int lockTry = 0; //The number of try to succeed in a good response of the service
        int maxTry = 4; //The maximum number of allowed tries
        while (lockTry < maxTry) {
            RJMC.callService("mod_vpl_evaluate");
            //We need to wait for the evaluation to finish
            synchronized (lock) {
                lock.wait(waitTime);
            }
            //Retrieve the results of the evaluation
            jsonFile = RJMC.callService("mod_vpl_get_result");

            //If the return object is a JSON with nothing in the evaluation or the grade, we try the process one more time and wait one more second
            if (jsonFile.getString("evaluation").equals("") || jsonFile.getString("grade").equals("")) {
                lockTry++;
                if (lockTry == maxTry) {
                    CaseineNotifier.notifyWarning(currentProject, "The Evaluation Failed", "Too many tries to let the API evaluate the exercise");
                }
                waitTime += 2000; //We wait a little more for the next time
            } else { //If not, everything is okay ! We can exit the while loop
                lockTry = maxTry;
            }
        }
        return jsonFile;
    }

    /**
     * Wait for evaluation result using a websocket.
     *
     * @param RJMC Moodle client used to create websocket (web socket is created in this method).
     * @return A {@code JsonObject} containing the results of the evaluation.
     * @throws VplConnectionException    En error occurred within the interaction with the web service
     * @throws MoodleWebServiceException In case an error happened in the moodle client.
     * @throws AlreadyTreatedException   An exception was thrown and execution flow should not be continued. Yet nothing else should be done about this exception as it has already been treated.
     */
    private JsonObject webSocketWait(RestJsonMoodleClient RJMC) throws VplConnectionException, MoodleWebServiceException, AlreadyTreatedException {
        JsonObject jsonFile;
        jsonFile = RJMC.callService("mod_vpl_evaluate");
        //We listen to the web socket
        try {
            listenToWebSocket(jsonFile.getString("smonitorURL"));
        } catch (VplConnectionException e) {
            if (e.getMessage().contains("received:")) {
                CaseineNotifier.notifyError(currentProject, "ERROR : VPL Connection Issue", "Try to change the evaluation method in the plugin settings (from recommended to compatibility)\nError message: " + e.getMessage(), new NotificationAction[]{
                        new NotificationAction("Open Caseine settings") {
                            @Override
                            public void actionPerformed(@NotNull AnActionEvent e, @NotNull Notification notification) {
                                ShowSettingsUtil.getInstance().showSettingsDialog(currentProject, SettingsConfigurable.class);
                            }
                        }
                });
                throw new AlreadyTreatedException();
            }
        }
        //Retrieve the results of the evaluation
        jsonFile = RJMC.callService("mod_vpl_get_result");
        return jsonFile;
    }

    /**
     * Change the "projectName".iml file which allow the project to be considered like a Java Project
     * and in the end, allow to execute the files of the exercise locally.
     *
     * @param imlFile the file named from the project name with the extension .iml (example: untitled.iml)
     * @return true if all the process was a success, false if not
     */
    boolean changeIML(File imlFile) {
        //Read the file
        byte[] input;
        try (FileInputStream fis = new FileInputStream(imlFile)) {
            input = fis.readAllBytes();
        } catch (IOException e) {
            return false;
        }
        //Transform the array of bytes in a string
        String fileString = new String(input);
        //This element is important, we need to add some lines after this
        String searchElement = "<exclude-output />";
        //The new lines which will be added to the file
        String newElement = "<content url=\"file://$MODULE_DIR$\">\n" +
                    "  <sourceFolder url=\"file://$MODULE_DIR$\" isTestSource=\"false\" />\n" +
                    "</content>\n" +
                    "<orderEntry type=\"inheritedJdk\" />\n";

        //Search if there is not already the newElement, if not, we can add it
        if (!fileString.contains(newElement)) {
            //Cut the string in half, the first part will contain the searchElement
            int indexSearch = fileString.indexOf(searchElement);
            int j = fileString.indexOf("\n", indexSearch) + 1;
            String before = fileString.substring(0, j);
            String after = fileString.substring(j);

            //Write in the file with the combination of the first original part, the new element and the second original part
            try (FileOutputStream fos = new FileOutputStream(imlFile)) {
                byte[] output = (before + newElement + after).getBytes(StandardCharsets.UTF_8);
                fos.write(output);
            } catch (IOException e) {
                return false;
            }
            return true;
        }
        return true;
    }


    /**
     * Listen to the given url via websocket during evaluation.
     * This method originates from the Caseine plugin for Eclipse source code.
     *
     * @param monitorUrl URL to listen to.
     * @throws VplConnectionException If anything goes wrong or unexpected when listening to execution server.
     * @author Christophe Saint-Marcel, Astor Bizard
     */
    private void listenToWebSocket(String monitorUrl) throws VplConnectionException {
        // Set up a curl execution to listen to the web socket.

        // Curl needs http/https instead of ws/wss
        if (monitorUrl.startsWith("ws")) {
            monitorUrl = "http" + monitorUrl.substring(2);
        }

        // Find host name (remove protocol and port / path)
        int hostIndex = monitorUrl.indexOf("://") + 3;
        String host = monitorUrl.substring(hostIndex, monitorUrl.indexOf(":", hostIndex));

        String command = "curl --no-buffer --ssl-no-revoke --header \"Connection: Upgrade\" --header \"Upgrade: websocket\" " +
                "--header \"Host:" + host + "\" --header \"Sec-WebSocket-Key: " + UUID.randomUUID() + "\" " +
                "--header \"Sec-WebSocket-Version: 13\" " + monitorUrl;

        String lastMessage = "";
        String curlMessage = "";
        try {
            Process process = Runtime.getRuntime().exec(command);
            InputStreamReader r = new InputStreamReader(process.getInputStream());
            int c;
            // Read curl data character by character
            while ((c = r.read()) != -1) {
                if (Character.getType(c) == Character.CONTROL || Character.getType(c) == Character.OTHER_SYMBOL) {
                    // End of line
                    int indexOfMessage = curlMessage.indexOf("message:");
                    if (indexOfMessage > -1) {
                        // Display the status message on the monitor.
                        String message = curlMessage.substring(indexOfMessage + 8);
                        if (message.length() > 0) {
                            message = Character.toUpperCase(message.charAt(0)) + message.substring(1);
                        }
                        //this.monitor.setTaskName(message);
                        lastMessage = message;
                    }
                    curlMessage = "";
                }
                curlMessage += (char) c;
                if (curlMessage.endsWith("retrieve:")) {
                    // "retrieve:" marker received: evaluation is finished, stop execution and return.
                    process.destroy();
                    return;
                }
            }
        } catch (IOException e) {
            throw new VplConnectionException(e);
        }
        String errorMsg = "Unexpected end of communication with execution server.";
        if (lastMessage.length() > 0) {
            errorMsg += "\nLast message received: " + lastMessage;
        }
        if (curlMessage.trim().length() > 0) {
            errorMsg += "\nLast bytes received: " + curlMessage.trim();
        }
        throw new VplConnectionException(new IOException(errorMsg));

    }
}
