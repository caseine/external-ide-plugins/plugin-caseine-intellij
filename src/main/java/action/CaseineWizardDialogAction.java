/*
 * Copyright (C) 2022 Joshua Monteiller, Astor Bizard, Christophe Saint-Marcel
 *
 * Caseine Plugin for IntelliJ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Caseine Plugin for IntelliJ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package action;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import com.intellij.ui.wizard.WizardModel;
import com.intellij.ui.wizard.WizardNavigationState;
import com.intellij.ui.wizard.WizardStep;
import module.CaseineModuleWizardFinalStep;
import module.CaseineModuleWizardStep;
import org.jetbrains.annotations.NotNull;


import javax.swing.*;

/**
 * This class serves to act as a button that will create a new project. This class is used to overcome the PyCharm problem
 *
 * @author Joshua Monteiller
 */
public class CaseineWizardDialogAction extends AnAction {

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        Project currentProject = e.getProject();
        //Create a WizardModel that will serve to structure the code
        WizardModel wizardModel = new WizardModel("New Caseine Project");

        //Construct the Wizard, the first step is the Caseine one, the second is the empty project one
        CaseineModuleWizardStep wizardStep = new CaseineModuleWizardStep();
        CaseineModuleWizardFinalStep finalStep = new CaseineModuleWizardFinalStep();
        wizardModel.add(new WizardStep<>() {
            @Override
            public JComponent prepare(WizardNavigationState state) {
                return wizardStep.getComponent();
            }
        });
        //Construct the Wizard, the first step is the Caseine one, the second is the empty project one
        wizardModel.add(new WizardStep<>() {
            @Override
            public JComponent prepare(WizardNavigationState state) {
                return finalStep.getComponent();
            }
        });
        CaseineWizardDialog wizard = new CaseineWizardDialog(currentProject, true, wizardModel, finalStep);
        wizard.show();
    }

    @Override
    public void update(@NotNull AnActionEvent e) {
        e.getPresentation().setEnabled(true);
    }
}
