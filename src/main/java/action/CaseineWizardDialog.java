/*
 * Copyright (C) 2022 Joshua Monteiller, Astor Bizard, Christophe Saint-Marcel
 *
 * Caseine Plugin for IntelliJ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Caseine Plugin for IntelliJ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package action;

import com.intellij.openapi.application.Application;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.roots.ModifiableRootModel;
import com.intellij.openapi.roots.ModuleRootManager;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.wizard.WizardDialog;
import com.intellij.ui.wizard.WizardModel;
import module.CaseineModuleBuilder;
import module.CaseineModuleWizardFinalStep;
import org.jdom.JDOMException;
import service.PersistentStorage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static module.CaseineModuleBuilder.createNameProject;

/**
 * This class serves to open a new window, a new wizard. This wizard will help overcome some problems encountered in the plugin when used in PyCharm.
 * In PyCharm, the CaseineModuleBuilder, CaseineModuleType and CaseineModuleWindowStep are most of the time useless because PyCharm don't allow to add a new type of "New Project" like IntelliJ does
 *
 * @author Joshua Monteiller
 */
public class CaseineWizardDialog extends WizardDialog<WizardModel> {

    private final CaseineModuleWizardFinalStep caseineFinalStep;

    private final Project currentProject;

    public CaseineWizardDialog(Project project, boolean canBeParent, WizardModel model, CaseineModuleWizardFinalStep finalStep) {
        super(project, canBeParent, model);
        currentProject = project;
        this.caseineFinalStep = finalStep;
    }

    /**
     * This method is called when the step in the wizard is changed, with the Next or the Previous button
     */
    @Override
    public void onStepChanged() {
        super.onStepChanged();
        String path = currentProject.getBasePath();
        //Find the index of the last file separator. If somehow, it failed, we can test to find the index with the know default file separator of Windows and Linux systems
        assert path != null;
        int lastIndex = path.lastIndexOf(File.separator);
        if (lastIndex == -1) {
            int a = path.lastIndexOf("/");
            int b = path.lastIndexOf("\\");
            lastIndex = Math.max(a, b);
        }

        path = path.substring(0, lastIndex);
        String newName = createNameProject(path);
        //Change the path and the name of the project
        if (!newName.equals("")) {
            if (!path.contains(newName)) {
                caseineFinalStep.setLocation(path + "/" + newName);
            }
            caseineFinalStep.setName(newName);
        }
    }


    /**
     * This method is called when the user clicks on the Finish button and will create the Caseine project
     */
    @Override
    public void onWizardGoalAchieved() {

        String name = caseineFinalStep.getName();
        String path = caseineFinalStep.getLocation();

        //Create the new directory
        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        //Create the file used by IntelliJ to set up the project
        createIdeaFiles(name, path);
        //Close the wizard
        super.onWizardGoalAchieved();

        //Ask the user if he wants to open the project
        ProjectManager projectManager = ProjectManager.getInstance();
        Project project;
        try {
            project = projectManager.loadAndOpenProject(path);
        } catch (IOException | JDOMException e) {
            System.out.println("ERROR: " + e.getMessage());
            return;
        }

        //This class is used to call the CaseineModuleBuilder. If CaseineModuleBuilder is not called inside a Runnable class, it causes error messages
        Project newProject = project;
        class CaseineModuleRunnable implements Runnable {

            @Override
            public void run() {
                ModuleManager moduleManager;
                //If the user chooses to open the new project, create it
                if (newProject != null) {
                    moduleManager = ModuleManager.getInstance(newProject);
                    String moduleFilePath = path + "/" + name + ".iml";
                    Module module = moduleManager.newModule(moduleFilePath, "JAVA_MODULE");
                    ModifiableRootModel modifiableModel = ModuleRootManager.getInstance(module).getModifiableModel();

                    CaseineModuleBuilder cmb = new CaseineModuleBuilder();
                    cmb.setupRootModel(modifiableModel);
                }
            }
        }
        //Launch the Runnable class
        CaseineModuleRunnable caseineModuleRunnable = new CaseineModuleRunnable();
        Application application = ApplicationManager.getApplication();
        if (application.isDispatchThread()) {
            application.runWriteAction(caseineModuleRunnable);
        } else {
            application.invokeLater(() -> application.runWriteAction(caseineModuleRunnable));
        }

        //Change the state of the boolean, to inform that a new project is in progress of be created
        PersistentStorage state = PersistentStorage.getInstance();
        state.newProjectStatus = true;

    }

    /**
     * Create the files needed for the project to be considered as a IntelliJ project
     *
     * @param name of the project
     * @param path of the project
     */
    private void createIdeaFiles(String name, String path) {
        //Content of the two files that will be created. The two serve to configure the project and let it be recognized by IntelliJ and others IDEs as a project and not a simple directory of files
        final String imlContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<module type=\"CASEINE_TYPE\" version=\"4\">\n" +
                "  <component name=\"NewModuleRootManager\" inherit-compiler-output=\"true\">\n" +
                "    <exclude-output />\n" +
                "    <content url=\"file://$MODULE_DIR$\">\n" +
                "      <sourceFolder url=\"file://$MODULE_DIR$\" isTestSource=\"false\" />\n" +
                "    </content>\n" +
                "    <orderEntry type=\"inheritedJdk\" />\n" +
                "    <orderEntry type=\"sourceFolder\" forTests=\"false\" />\n" +
                "  </component>\n" +
                "</module>";

        final String moduleXmlContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<project version=\"4\">\n" +
                "  <component name=\"ProjectModuleManager\">\n" +
                "    <modules>\n" +
                "      <module fileurl=\"file://$PROJECT_DIR$/" + name + ".iml\" filepath=\"$PROJECT_DIR$/" + name + ".iml\" />\n" +
                "    </modules>\n" +
                "  </component>\n" +
                "</project>";

        //Create the .iml file if it doesn't exist yet and modify its content
        File imlFile = new File(path + File.separator + name + ".iml");
        try {
            if (!imlFile.exists()) {
                imlFile.createNewFile();
            }
            try (FileOutputStream fos = new FileOutputStream(imlFile)) {
                fos.write(imlContent.getBytes(StandardCharsets.UTF_8));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        //Create the .idea directory if it doesn't exist yet
        File ideaDir = new File(path + File.separator + ".idea");
        if (!ideaDir.exists()) {
            ideaDir.mkdirs();
        }
        //Create the module.xml file if it doesn't exist yet and modify its content
        File moduleXmlFile = new File(path + File.separator + ".idea" + File.separator + "modules.xml");
        try {
            if (!moduleXmlFile.exists()) {
                moduleXmlFile.createNewFile();
            }
            try (FileOutputStream fos = new FileOutputStream(moduleXmlFile)) {
                fos.write(moduleXmlContent.getBytes(StandardCharsets.UTF_8));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        //Refresh the module.xml when it's possible
        VirtualFile virtualFile = LocalFileSystem.getInstance().findFileByIoFile(moduleXmlFile);
        if (virtualFile != null) {
            ApplicationManager.getApplication().invokeLater(() -> virtualFile.refresh(false, false));
        }
    }


}
