/*
 * Copyright (C) 2022 Joshua Monteiller, Astor Bizard, Christophe Saint-Marcel
 *
 * Caseine Plugin for IntelliJ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Caseine Plugin for IntelliJ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package service;

import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import module.CaseineModuleBuilder;
import vplwsclient.FileUtils;
import vplwsclient.RestJsonMoodleClient;
import vplwsclient.VplFile;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.VplConnectionException;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonValue;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * This class is used to bridge between the plugin and the RestJsonMoodleClient.
 * It allows the plugin to get the name, description and other things from the JSON file much easier.
 *
 * @author Joshua Monteiller
 */
public class ServiceGetter {

    private RestJsonMoodleClient RJMC;
    private JsonObject jsonInfo;
    private boolean fail;

    private String vplID;
    private String token;
    private String url;
    private final String path;


    /**
     * Construct a new ServiceGetter with the variable stored in the Storage
     *
     * @param path the path of the file which contains the VPL ID
     */
    public ServiceGetter(String path) {
        this.path = path;

        PersistentStorage state = PersistentStorage.getInstance();
        boolean existVpl = new File(path + ".moodlevpl").exists();
        if (existVpl) {
            vplID = state.getProjectVplID(path);
        } else {
            vplID = state.vplID;
        }
        token = state.userToken;
        url = state.caseineRadioButtonSelected ? state.caseineURL : state.customURL;

        RJMC = new RestJsonMoodleClient(vplID, token, url);
        try {
            jsonInfo = RJMC.callService("mod_vpl_info");
            fail = false;
        } catch (MoodleWebServiceException | VplConnectionException e) {
            fail = true;
        }
    }

    /**
     * Returns the client used in the class to be used manually by other methods
     *
     * @return RJMC the RestJsonMoodleClient used in the ServiceGetter class
     */
    public RestJsonMoodleClient getRJMC() {
        updateModified();
        return RJMC;
    }

    /**
     * This method calls the API to have the name of the exercise.
     *
     * @return the String which represents the name of the exercise
     */
    public String getExerciseName() {
        updateModified();
        return fail ? "" : jsonInfo.getString("name");
    }

    /**
     * This method calls the API to have the description of the exercise.
     *
     * @return the String which represents the description of the exercise
     */
    public String getDescription() {
        updateModified();
        return fail ? "Not a Caseine Project" : "<html>\n<head></head>\n<body>\n" + jsonInfo.getString("intro") + "\n</body>\n</html>";
    }

    /**
     * This method calls the API to retrieve all the initial files of the exercise.
     *
     * @return the array of the files needed for the exercise
     */
    public JsonArray getReqFiles() {
        updateModified();
        return jsonInfo.getJsonArray("reqfiles");
    }

    /**
     * This method calls the API to retrieve all the files of the user for the exercise.
     *
     * @return the array of the files needed for the exercise
     */
    public JsonArray getFiles() {
        JsonObject jsonOpen;
        updateModified();
        try {
            jsonOpen = RJMC.callService("mod_vpl_open");
        } catch (MoodleWebServiceException | VplConnectionException e) {
            throw new RuntimeException(e);
        }
        return jsonOpen.getJsonArray("files");
    }

    /**
     * Returns a boolean which represents if the connection with the service has failed or not
     *
     * @return a boolean, true the connection has failed somehow, false everything is okay
     */
    public boolean haveFailed() {
        return fail;
    }

    /**
     * Verifies that the ID of the exercise, the user token and the url haven't changed.
     * If they have, reinitialize the variables, make a new RestJsonMoodleClient from the new variables and try to retrieve a new 'info' JSON file.
     */
    public void updateModified() {
        PersistentStorage state = PersistentStorage.getInstance();
        String stateUrl = state.caseineRadioButtonSelected ? state.caseineURL : state.customURL;
        vplID = state.getProjectVplID(path);
        token = state.userToken;
        url = stateUrl;
        RJMC = new RestJsonMoodleClient(vplID, token, url);
        try {
            jsonInfo = RJMC.callService("mod_vpl_info");
            fail = false;
        } catch (MoodleWebServiceException | VplConnectionException e) {
            fail = true;
        }
    }

    /**
     * Return if the path to the special file have changed
     *
     * @param newPath the "new" path of the special file
     * @return a boolean, if the path have changed, return true, if not return false
     */
    public boolean isPathModified(String newPath) {
        return !newPath.equals(path);
    }


    /**
     * Pull all the initial files or all the previously pushed files of the exercise.
     * It depends on the JsonArray given in argument.
     *
     * @param JArray the JsonArray, if it comes from {@link #getReqFiles()}, it represents all the initial files and if it comes from {@link #getFiles()}, it represents all the previously pushed files
     * @throws IOException If an error occurred during the interaction with local files.
     */
    public static void retrieveFiles(JsonArray JArray, String path) throws IOException {
        LocalFileSystem fileSystem = LocalFileSystem.getInstance();
        List<String> excludedFiles = FileUtils.scanExcludedList(path);
        excludedFiles.add(CaseineModuleBuilder.SPECIAL_FILE_NAME);
        excludedFiles.add(".idea/modules.xml");
        excludedFiles.add("*.iml");

        // First delete all files
        for (File file : Objects.requireNonNull(new File(path).listFiles())) {
            if (!FileUtils.isExcluded(file.getName(), excludedFiles))
                FileUtils.deleteIncludedFolder(file, path, excludedFiles);
        }

        // Then recover from remote
        for (JsonValue jsonValue : JArray) {
            //Retrieve the i-st file in a Json format
            JsonObject JFile = (JsonObject) jsonValue;
            VplFile vplFile = new VplFile(JFile);

            // Do not import files registered in .vplignore
            if (FileUtils.isExcluded(vplFile.getFullName(), excludedFiles))
                continue;

            //Write the file in the folder specified by the path
            String fileName = vplFile.getFullName();
            File file = new File(path + fileName);

            //Retrieve the name of the directory of the file
            int index = fileName.lastIndexOf('/');
            String dirName = fileName.substring(0, index + 1);
            File directory = new File(path + dirName);

            //If the directories do not exist, we create it
            if (!directory.exists())
                directory.mkdirs();
            FileOutputStream fos = new FileOutputStream(file);
            vplFile.write(fos);

            //Refresh the file's content
            VirtualFile virtualFile = fileSystem.findFileByIoFile(file);
            if (virtualFile != null) {
                virtualFile.refresh(false, false);
            }
        }

        //Refresh the project's root
        VirtualFile virtualFile = fileSystem.findFileByIoFile(new File(path));
        if (virtualFile != null) {
            virtualFile.refresh(false, false);
        }
    }
}
