package evaluation;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.StatusBarWidget;
import module.CaseineModuleType;
import org.jetbrains.annotations.Nullable;
import service.ServiceGetter;
import ui.icons.CaseineIcons;
import vplwsclient.RestJsonMoodleClient;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.VplConnectionException;

import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.swing.*;

/**
 * The EvaluationWidgetPresentation class represents a presentation for the evaluation widget.
 * It implements the StatusBarWidget.MultipleTextValuesPresentation interface.
 *
 * @see StatusBarWidget.MultipleTextValuesPresentation
 */
public class EvaluationWidgetPresentation implements StatusBarWidget.MultipleTextValuesPresentation {

    private static Integer freeEvaluations;
    private static Integer nbEvaluations;
    private static String reductionByEvaluation;

    private final Project project;

    /**
     * Constructor of the EvaluationWidgetPresentation class
     *
     * @param project the project
     */
    public EvaluationWidgetPresentation(Project project) {
        this.project = project;
        updateEvaluationCounter(project);
    }

    /**
     * Call {@code mod_vpl_subrestrictions} to update the Evaluation Counter.
     *
     * @param project The current project.
     */
    public static void updateEvaluationCounter(Project project) {
        ServiceGetter serv = new ServiceGetter(project.getBasePath() + "/");
        RestJsonMoodleClient RJMC = serv.getRJMC();
        JsonObject values;
        try {
            values = RJMC.callService("mod_vpl_subrestrictions");
        } catch (VplConnectionException | MoodleWebServiceException e) {
            freeEvaluations = null;
            nbEvaluations = null;
            reductionByEvaluation = null;
            return;
        }

        JsonValue freeEvaluationsValue = values.get("freeevaluations");
        if (freeEvaluationsValue.getValueType() == JsonValue.ValueType.NUMBER) {
            freeEvaluations = Integer.parseInt(freeEvaluationsValue.toString());
        } else {
            freeEvaluations = null;
        }

        JsonValue nEvaluationsValue = values.get("nevaluations");
        if (nEvaluationsValue.getValueType() == JsonValue.ValueType.NUMBER) {
            nbEvaluations = Integer.parseInt(nEvaluationsValue.toString());
        } else {
            nbEvaluations = null;
        }

        JsonValue reductionByEvaluationValue = values.get("reductionbyevaluation");
        if (reductionByEvaluationValue.getValueType() == JsonValue.ValueType.STRING) {
            reductionByEvaluation = reductionByEvaluationValue.toString().substring(1, reductionByEvaluationValue.toString().length() - 1);
            if (!reductionByEvaluation.endsWith("%")) {
                reductionByEvaluation += "pts";
            }
        } else {
            reductionByEvaluation = null;
        }
    }

    /**
     * Returns the text to display
     *
     * @return the text to display
     */
    @Nullable
    @Override
    public String getSelectedValue() {
        if (!CaseineModuleType.isCaseineProject(project))
            return "";
        if (freeEvaluations == null || nbEvaluations == null || reductionByEvaluation == null) {
            return "No evaluation limitation";
        }
        return nbEvaluations + "/" + freeEvaluations + " (-" + reductionByEvaluation + ")";
        /*"1/5 (-5pts)"*/
    }

    /**
     * Returns the text to display in the tooltip
     *
     * @return the text to display in the tooltip
     */
    @Nullable
    @Override
    public String getTooltipText() {
        if (!CaseineModuleType.isCaseineProject(project))
            return "";
        if (freeEvaluations == null) {
            return "No evaluation limitation";
        }
        if (freeEvaluations - nbEvaluations < 0) {
            return "0 free evaluation(s) remaining,<br> then -" + reductionByEvaluation + " per evaluation"
                    /*"0 free evaluation(s) remaining,<br> then -5pts per evaluation"*/;
        }
        return freeEvaluations - nbEvaluations + " free evaluation(s) remaining,<br> then -" + reductionByEvaluation + " per evaluation"
                /*"5 free evaluation(s) remaining,<br> then -5pts per evaluation"*/;
    }

    /**
     * Returns the icon to display
     *
     * @return the icon to display
     */
    @Nullable
    @Override
    public Icon getIcon() {
        if (freeEvaluations != null)
            return CaseineIcons.Eval;
        else
            return null;
    }
}
