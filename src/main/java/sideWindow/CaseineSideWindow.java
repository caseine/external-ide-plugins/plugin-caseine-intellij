/*
 * Copyright (C) 2022 Joshua Monteiller, Astor Bizard, Christophe Saint-Marcel
 *
 * Caseine Plugin for IntelliJ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Caseine Plugin for IntelliJ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package sideWindow;

import com.intellij.ide.ui.laf.darcula.ui.DarculaProgressBarUI;
import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.ActionToolbar;
import com.intellij.openapi.actionSystem.DefaultActionGroup;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.ui.Gray;
import com.intellij.ui.treeStructure.Tree;
import service.ServiceGetter;
import ui.icons.CaseineColors;
import vplwsclient.RestJsonMoodleClient;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.VplConnectionException;

import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.swing.*;
import javax.swing.plaf.basic.BasicProgressBarUI;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static sideWindow.TestResult.*;

/**
 * The class CaseineSideWindow represent the window that appeared normally on the right side of the IntelliJ window.
 * The plugin will use this class to show to the user the description of the exercise and the result of it once it had been evaluated.
 * Use a .form file to create the UI.
 *
 * @author Joshua Monteiller
 */
public class CaseineSideWindow {

    /////////User Interface////////
    @SuppressWarnings("unused")
    private JPanel myToolWindowContent;
    @SuppressWarnings("unused")
    private JTextPane exerciseNameTextPane;
    @SuppressWarnings("unused")
    private JTextPane descriptionExercise;
    @SuppressWarnings("unused")
    public JTree resultsTree;
    @SuppressWarnings("unused")
    private JProgressBar gradeProgressBar;
    @SuppressWarnings("unused")
    private JTextField gradeResult;
    @SuppressWarnings("unused")
    private JTextField nbFailures;
    @SuppressWarnings("unused")
    private JTextField nbTests;
    @SuppressWarnings("unused")
    private JTextField nbErrors;
    @SuppressWarnings("unused")
    private JButton reloadButton;
    @SuppressWarnings("unused")
    private JTextPane messageResult;
    @SuppressWarnings("unused")
    private JTabbedPane TabbedPane;
    @SuppressWarnings("unused")
    private JPanel descriptionPanel;
    @SuppressWarnings("unused")
    private JPanel resultPanel;
    @SuppressWarnings("unused")
    private JPanel layout2;
    @SuppressWarnings("unused")
    private JScrollPane treeScrollPane;
    @SuppressWarnings("unused")
    private JScrollPane messageScrollPane;
    @SuppressWarnings("unused")
    private JScrollPane descriptionScrollPane;
    @SuppressWarnings("unused")
    private JToolBar caseineToolBar;


    /////////Variables////////
    /**
     * The array of the different tests, represented by the class TestResult
     */
    private List<TestResult> listTestResult;

    /**
     * The tree model of the JTree which is used to show all the tests and their result
     */
    private DefaultTreeModel treeModel;

    /**
     * A class which is used to get all the important stuff from the web API
     */
    private final ServiceGetter servGet;


    /**
     * The constructor which is used to set multiple texts and an action listener.
     * However, the JTree is initialized in a different method {@link #createUIComponents() createUIComponents}
     *
     * @param path the path to the special file which contains the VPL ID
     */
    public CaseineSideWindow(String path) {
        //Initialize the ServiceGetter
        this.servGet = new ServiceGetter(path);

        //Show the exercise name
        exerciseNameTextPane.setText(servGet.getExerciseName());

        // Add action bar
        final ActionManager actionManager = ActionManager.getInstance();
        DefaultActionGroup actionGroup = new DefaultActionGroup("CASEINE_ACTION", true);
        actionGroup.add(ActionManager.getInstance().getAction("caseine.reset"));
        actionGroup.add(ActionManager.getInstance().getAction("caseine.pull"));
        actionGroup.add(ActionManager.getInstance().getAction("caseine.push"));
        actionGroup.add(ActionManager.getInstance().getAction("caseine.evaluate"));
        ActionToolbar actionToolbar = actionManager.createActionToolbar("CASEINE_TOOLBAR", actionGroup, false);
        actionToolbar.setOrientation(SwingConstants.HORIZONTAL);
        actionToolbar.setTargetComponent(caseineToolBar);
        caseineToolBar.add(actionToolbar.getComponent());

        //The description of the exercise
        descriptionExercise.setText(servGet.getDescription());

        //Add an action listener of the reload button
        reloadButton.addActionListener(e -> {
            if (!CaseineSideWindowFactory.changeCsw(path)) {
                //If the button was pressed without change of VPL ID, reload the description and name of the exercise
                descriptionExercise.setText(servGet.getDescription());
                exerciseNameTextPane.setText(servGet.getExerciseName());
                initResults(path);
            }
        });

        initResults(path);
    }

    /**
     * Set the result panel of the Caseine SideWindow.
     *
     * @param path Path to the project root.
     */
    public void initResults(String path) {
        //Set a different text depending on if the connection with the API have failed or not
        if (servGet.haveFailed()) {
            gradeResult.setText("Not a Caseine project");
            setResultGrade("Not a Caseine project");
            setResultsTree("", "");
            //Hide the different texts from the user before the evaluation
            setTextsVisible(false);
            return;
        }

        // Retrieve last evaluation
        ServiceGetter service = new ServiceGetter(path);
        RestJsonMoodleClient RJMC = service.getRJMC();
        JsonObject lastEvaluation;
        try {
            lastEvaluation = RJMC.callService("mod_vpl_get_last_evaluation");
        } catch (VplConnectionException | MoodleWebServiceException e) {
            gradeResult.setText("Not yet evaluated...");
            setTextsVisible(false);
            setResultGrade("Not yet evaluated...");
            setResultsTree("", "");
            return;
        }

        if (lastEvaluation.get("grade").getValueType() == JsonValue.ValueType.NULL) {
            gradeResult.setText("Not yet evaluated....");
            setTextsVisible(false);
            return;
        }

        //The project has already been evaluated, display the results of the last evaluation.
        JsonValue jsonDate = lastEvaluation.get("timesubmitted");
        String date;
        if (jsonDate.getValueType() == JsonValue.ValueType.NUMBER) {
            JsonNumber jsonNumberDate = lastEvaluation.getJsonNumber("timesubmitted");
            if (jsonNumberDate == null) {
                date = "Unknown date";
            } else if (jsonNumberDate.longValue() == 0) {
                date = "Ancient evaluation";
            } else {
                date = (new SimpleDateFormat()).format(new Date(jsonNumberDate.longValue() * 1000));
            }
        } else
            date = "Unknown date";
        setResultsTree(lastEvaluation.getString("evaluation"), date);
        setResultGrade(lastEvaluation.getString("grade"));
        //Expand the tree later at the end of the execution of this method
        setNodeExpanded(resultsTree, (DefaultMutableTreeNode) resultsTree.getModel().getRoot());
        setTextsVisible(true);
    }

    /**
     * Create the JTree which will be used to show to the user the different tests and their result.
     */
    private void createUIComponents() {
        //Make a tree with only a root. It will be expanded when an evaluation will happen
        DefaultMutableTreeNode top = new DefaultMutableTreeNode("");
        resultsTree = new Tree(top);
        treeModel = new DefaultTreeModel(top);

        //Add a listener for the tree when a node is selected
        resultsTree.addTreeSelectionListener(e -> nodeSelected());
    }


    //////______________SETTERS______________//////

    /**
     * See {@link #setResultGrade(String, boolean)} with determinate progress bar on default.
     *
     * @param resultGrade The string which represents the grade message given by the API.
     */
    public void setResultGrade(String resultGrade) {
        setResultGrade(resultGrade, false);
    }

    /**
     * A parser which parse the grade found in the JSON response after the call to get_result of the API.
     *
     * @param resultGrade The string which represents the grade message given by the API
     */
    public void setResultGrade(String resultGrade, boolean indeterminateProgressBar) {
        gradeProgressBar.setIndeterminate(indeterminateProgressBar);
        if (indeterminateProgressBar) {
            gradeProgressBar.setForeground(null);
            gradeProgressBar.setUI(new DarculaProgressBarUI());
        }

        //If the grade doesn't contain a ':', we will treat it as plain text.
        if (!resultGrade.contains(":") || !resultGrade.contains("/")) {
            String finalResultGrade = resultGrade;
            ApplicationManager.getApplication().invokeAndWait(() -> {
                gradeResult.setText(finalResultGrade);
                gradeProgressBar.setValue(0);
                gradeProgressBar.setString("");
                if (!indeterminateProgressBar) {
                    gradeProgressBar.setBackground(CaseineColors.NONE);
                }
            });
            return;
        }

        //Now we need to parse this message to have the exact grade and so, to set the progress bar
        //We search for the first ':', which is often found before the grade
        int index = resultGrade.indexOf(':');
        String text = (resultGrade.substring(index + 1)).trim();
        //We now search for the '/' which separate the user grade and the maximum grade
        index = text.indexOf('/');

        String grade = (text.substring(0, index))
                .trim()
                .replace(",", "."); //The grade
        String maxGrade = (text.substring(index + 1))
                .trim()
                .replace(",", "."); //The maximum grade

        //In case the grade is superior to the maximum grade
        if (Float.parseFloat(grade) > Float.parseFloat(maxGrade)) {
            resultGrade = resultGrade.replace(grade, maxGrade);
            grade = maxGrade;
        }

        String finalGrade = grade;
        String finalResultGrade1 = resultGrade;
        ApplicationManager.getApplication().invokeAndWait(() -> {
            // Set progress bar to illustrate given grade.
            gradeResult.setText(finalResultGrade1);
            gradeProgressBar.setBackground(Gray._162);
            gradeProgressBar.setForeground(CaseineColors.GREEN);
            gradeProgressBar.setUI(new BasicProgressBarUI());
            gradeProgressBar.setMinimum(0);
            gradeProgressBar.setMaximum(maxGrade.equals("") ? 100 : (int) Float.parseFloat(maxGrade));
            gradeProgressBar.setValue(finalGrade.equals("") ? 0 : Math.round(Float.parseFloat(finalGrade)));
            gradeProgressBar.setString(text);
        });
    }


    /**
     * Create the tree for the JTree, which will be used to show to the user the different tests and their result.
     *
     * @param eval String got with the JSON response of get_result
     */
    public void setResultsTree(String eval, String date) {
        if (eval == null || eval.equals("")) {
            ApplicationManager.getApplication().invokeAndWait(() -> setTextsVisible(false));
            messageResult.setText("");
            return;
        }

        //Initialize the tree
        DefaultMutableTreeNode top = new DefaultMutableTreeNode(new TestResult("<html>All tests <font color=\"#3e86a0\">[" + date + "]</font></html>"));
        treeModel = new DefaultTreeModel(top);

        //Add a listener for the tree when a node is selected
        resultsTree.addTreeSelectionListener(e -> nodeSelected());

        //Call a parser to retrieve the results of the test in a form of an array
        listTestResult = getTestResults(eval);

        int nbT = listTestResult.size(); //Number of tests, which is also the size of the array
        int nbE = 0; //Number of errors
        int nbF = 0; //Number of failures

        //Count the number of faults and errors
        for (TestResult tr : listTestResult) {
            String result = tr.getSuccess();
            if (!result.equals(SUCCESS)) {
                if (result.equals(FAILURE)) {
                    nbF++;
                } else {
                    nbE++;
                }
            }
        }

        //Set the texts for the number of tests, errors and failures and set it visible
        nbTests.setText(nbT + " Tests");
        nbErrors.setText("Errors: " + nbE);
        nbFailures.setText("Failures: " + nbF);
        setTextsVisible(true);

        //Make a new array which represents all the primary nodes of the JTree
        List<DefaultMutableTreeNode> listNodes = new ArrayList<>();

        //For all the test results
        for (TestResult result : listTestResult) {

            //We look if it belongs to a specific node other than the root
            //If not, we add it directly in the list
            if (result.getNodeName() == null) {
                listNodes.add(new DefaultMutableTreeNode(result));
                continue;
            }

            // We search if the node is already in the array or not
            boolean added = false;
            for (int i = 0; i < listNodes.size(); i++) {
                DefaultMutableTreeNode nodeParsed = listNodes.get(i);
                TestResult resultParsed = (TestResult) nodeParsed.getUserObject();

                if (!result.getNodeName().equals(resultParsed.getName()))
                    continue;

                // We found the node. Let's add it to our tree.
                nodeParsed.add(new DefaultMutableTreeNode(result));

                //If the actual node is not already in an Error state we need to change it's state.
                if (!resultParsed.getSuccess().equals(ERROR)) {
                    //If the node is in a success state, its next state will be the same as the test result
                    if (!result.getSuccess().equals(SUCCESS)) {
                        resultParsed = new TestResult(resultParsed.getName(), resultParsed.getNodeName(), resultParsed.getMessage(), result.getSuccess());
                    }

                    //Update the node depends on the result of the new test result and the previous ones
                    nodeParsed.setUserObject(resultParsed);
                }

                //Add the node to the list so for the next loop, this node can be used
                listNodes.set(i, nodeParsed);
                added = true;
                break;
            }
            if (!added) {
                DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(new TestResult(result.getNodeName()));
                //if it wasn't added (for multiple reasons), we had the child to the actual node
                newNode.add(new DefaultMutableTreeNode(result));
                listNodes.add(newNode);
            }
        }

        //We add everything in the root
        for (DefaultMutableTreeNode node : listNodes) {
            top.add(node);
        }

        //Set the tree model based on the root we have just created
        treeModel.setRoot(top);
        //Set the JTree on the model
        resultsTree.setModel(treeModel);
        //Initialize the message result
        messageResult.setText("");
        //Set the cell renderer of the tree, it is useful to show the real name of the test result, the linked icon of the result and to expand the tree
        resultsTree.setCellRenderer(new TestResultTreeCellRenderer());
    }


    /**
     * Set the node expansion to true to allow the tree to be completely expanded.
     * Recursive method.
     *
     * @param tree The JTree which needed to be expanded
     * @param node The actual node. In the first call of the method, it is the root of the tree. After that is all the children of the root
     */
    public void setNodeExpanded(JTree tree, DefaultMutableTreeNode node) {
        //Recursive call for every Tree Node
        for (TreeNode treeNode : Collections.list(node.children())) {
            setNodeExpanded(tree, (DefaultMutableTreeNode) treeNode);
        }
        //If it is the root or if the node has no children, we don't need to use expandPath
        if (node.isRoot() || (node.getChildCount() == 0)) return;
        //If the node is a success, don't expand it
        if (((TestResult) node.getUserObject()).getSuccess().equals(SUCCESS)) return;

        //Expand the node
        tree.expandPath(new TreePath(node.getPath()));
    }

    /**
     * Set the different texts visible or not, depends on the boolean.
     * Here the texts are the texts in the side window and represent the number of tests, of faults and of errors
     *
     * @param b true the texts are visible, false the texts are not
     */
    private void setTextsVisible(boolean b) {
        nbTests.setVisible(b);
        nbErrors.setVisible(b);
        nbFailures.setVisible(b);
        resultsTree.setVisible(b);
    }

    /**
     * Set the panel selected on the result panel.
     * This method is only called after a successful call in the Evaluate method of the class CaseineAction.
     */
    public void setSelectedTab() {
        ApplicationManager.getApplication().invokeAndWait(() -> TabbedPane.setSelectedIndex(1));
    }


    /////______________GETTERS______________//////

    /**
     * Returns the global panel which contained all the UI of the side window
     *
     * @return JPanel
     */
    public JPanel getContent() {
        return myToolWindowContent;
    }

    /**
     * A parser which parse the string in argument.
     * Will return an array of TestResult which are composed of a name, a node if it exists, a message if it exists, a String which indicated if the test was successful or not.
     * A TestResult represent the result of one test
     *
     * @param evaluation this string represents the string obtained in the JSON response of the API
     * @return List<TestResult> an array which contained all the test results
     */
    private List<TestResult> getTestResults(String evaluation) {
        //We process the text to cut it into an array
        List<String> listString = new ArrayList<>();

        //If the first char of the string is '-', we can skip it directly
        int begin = evaluation.startsWith("-") ? 1 : 0;
        int end = evaluation.indexOf("\n-", begin);
        //The way of parsing the test results is very simple,
        //Each new test result begins directly after the previous one and end with a line break and a hyphen
        while (end != -1) {
            String sub = evaluation.substring(begin, end);
            listString.add(sub);
            begin = end + 2;
            end = evaluation.indexOf("\n-", begin);
        }
        //We may have not found any more line breaks, but it doesn't mean we have all the test results
        String sub = evaluation.substring(begin);
        while (sub.endsWith("\n")) {
            sub = sub.substring(0, sub.length() - 1);
        }
        listString.add(sub);
        //Now we have an array with all the text of each result

        //An array of TestResult, which is each composed of a name, a node, a message and a boolean if it succeeds or not
        List<TestResult> listTestResult = new ArrayList<>();
        for (String resultText : listString) {
            //We skip the case of "Tests results" because it's not interesting to show to the user
            if (!resultText.equals("Tests results")) {
                TestResult tr = new TestResult();
                //Parse the text to create the TestResult based on it
                tr.parseText(resultText);

                //Everything is added to the list
                listTestResult.add(tr);
            }
        }

        return listTestResult;
    }


    //////______________LISTENERS______________//////


    /**
     * A listener for when a node of the JTree is selected.
     * If the node is a result text, we set the messageResult TextField with the appropriate message.
     */
    private void nodeSelected() {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) this.resultsTree.getLastSelectedPathComponent();
        if (node == null) return;

        boolean found = false;
        if (this.listTestResult != null) {
            for (TestResult tr : this.listTestResult) {
                Object userObject = node.getUserObject();
                if (userObject instanceof TestResult) {
                    if (((TestResult) userObject).getName().equals(tr.getName())) {
                        //If found the good node, show the message with a little indication if the test has succeeded or not
                        messageResult.setText(tr.getMessage());
                        found = true;
                        break;
                    }
                }
            }
        }
        //If the node selected is not a child, don't show any message
        if (!found) {
            messageResult.setText("");
        }
    }
}
