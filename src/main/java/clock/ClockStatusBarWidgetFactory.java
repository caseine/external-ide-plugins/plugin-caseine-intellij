/*
 * Copyright (C) 2023 GEILLER Valentin & GUEZI Yanis
 *
 * Caseine Plugin for IntelliJ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Caseine Plugin for IntelliJ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package clock;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.NlsContexts;
import com.intellij.openapi.wm.StatusBar;
import com.intellij.openapi.wm.StatusBarWidget;
import com.intellij.openapi.wm.StatusBarWidgetFactory;
import kotlinx.coroutines.CoroutineScope;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

/**
 * The ClockStatusBarWidgetFactory class represents a factory for creating instances of the ClockStatusBarWidget.
 * It implements the StatusBarWidgetFactory interface.
 *
 * @author GEILLER Valentin & GUEZI Yanis
 * @see StatusBarWidgetFactory
 */
public class ClockStatusBarWidgetFactory implements StatusBarWidgetFactory {

    /**
     * Returns the clock widget ID
     *
     * @return the clock widget ID
     */
    @Override
    public @NotNull @NonNls String getId() {
        return "Caseine-Clock";
    }

    /**
     * Returns the clock widget display name
     *
     * @return the clock widget display name
     */
    @Override
    public @NotNull @NlsContexts.ConfigurableName String getDisplayName() {
        return "Caseine Clock";
    }

    /**
     * Returns if the clock widget is available
     *
     * @param project the project
     * @return if the clock widget is available
     */
    @Override
    public boolean isAvailable(@NotNull Project project) {
        return true;
    }

    /**
     * Creates a clock widget
     *
     * @param project the project
     * @param scope   the scope
     * @return the clock widget
     */
    @Override
    public @NotNull StatusBarWidget createWidget(@NotNull Project project, @NotNull CoroutineScope scope) {
        return StatusBarWidgetFactory.super.createWidget(project, scope);
    }

    /**
     * Creates a clock widget
     *
     * @param project the project
     * @return the clock widget
     */
    @Override
    public @NotNull StatusBarWidget createWidget(@NotNull Project project) {
        return new ClockStatusBarWidget(project);
    }

    /**
     * Disposes the clock widget
     *
     * @param widget the clock widget
     */
    @Override
    public void disposeWidget(@NotNull StatusBarWidget widget) {
        StatusBarWidgetFactory.super.disposeWidget(widget);
    }

    /**
     * Returns if the clock widget can be enabled on the status bar
     *
     * @param statusBar the status bar
     * @return if the clock widget can be enabled on the status bar
     */
    @Override
    public boolean canBeEnabledOn(@NotNull StatusBar statusBar) {
        return true;
    }
}
