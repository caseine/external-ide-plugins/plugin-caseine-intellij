/*
 * Copyright (C) 2023 GEILLER Valentin & GUEZI Yanis
 *
 * Caseine Plugin for IntelliJ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Caseine Plugin for IntelliJ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package clock;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.StatusBarWidget;
import module.CaseineModuleType;
import org.jetbrains.annotations.Nullable;
import service.ServiceGetter;
import ui.icons.CaseineIcons;
import vplwsclient.RestJsonMoodleClient;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.VplConnectionException;

import javax.json.JsonValue;
import javax.swing.*;

/**
 * The ClockWidgetPresentation class represents a presentation for the clock widget.
 * It implements the StatusBarWidget.MultipleTextValuesPresentation interface.
 *
 * @author GEILLER Valentin & GUEZI Yanis
 * @see StatusBarWidget.MultipleTextValuesPresentation
 */
public class ClockWidgetPresentation implements StatusBarWidget.MultipleTextValuesPresentation {

    private Integer nbSeconds;
    private String name;
    /*
     * Frequency of the call to the api
     */
    private final int frequency = 1; // en s
    private final Project project;

    /**
     * Constructor of the ClockWidgetPresentation class
     * It will create 2 threads :
     * - One to display the time left
     * - One to call the api every ? seconds, defined by the frequency attribute
     *
     * @param project the project
     */
    public ClockWidgetPresentation(Project project) {
        this.project = project;
        nbSeconds = call();

        // Display
        new Thread(() -> {
            while (true) {

                try {
                    //noinspection BusyWait
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (nbSeconds == null) {
                    name = "There is no time limitation with this exercise";
                } else {
                    if (nbSeconds > 0) {
                        nbSeconds--;
                        name = "Time left : " + getFormattedTime(nbSeconds);
                    } else {
                        name = "Time is up";
                    }
                }
            }
        }).start();

        // Call api
        new Thread(() -> {
            while (true) {
                try {
                    //noinspection BusyWait
                    Thread.sleep(frequency * 1000);
                    nbSeconds = call();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }).start();
    }

    /**
     * Returns the text to display
     *
     * @return the text to display
     */
    @Nullable
    @Override
    public String getSelectedValue() {
        if (!CaseineModuleType.isCaseineProject(project))
            return "";
        return name;
    }

    /**
     * Returns the text to display in the tooltip
     *
     * @return the text to display in the tooltip
     */
    @Nullable
    @Override
    public String getTooltipText() {
        if (!CaseineModuleType.isCaseineProject(project))
            return "";
        return getSelectedValue();
    }

    /**
     * Returns the icon to display
     *
     * @return the icon to display
     */
    @Nullable
    @Override
    public Icon getIcon() {
        if (nbSeconds == null) {
            return null;
        }
        if (nbSeconds > 600) {
            return CaseineIcons.GreenTimer;
        }
        if (nbSeconds >= 60) {
            return CaseineIcons.OrangeTimer;
        }
        return CaseineIcons.RedTimer;
    }

    /**
     * Returns the text to display in the status bar for the given time
     * Example : 01:02:03
     *
     * @param time the time
     * @return the text to display in the status bar
     */
    private String getFormattedTime(int time) {
        int hour = time / 3600;
        int minute = (time % 3600) / 60;
        int second = time % 60;
        return String.format("%02d:%02d:%02d", hour, minute, second);
    }

    /**
     * Calls the api to get the time left
     *
     * @return the time left in seconds or null if there is no time limitation
     */
    private @Nullable Integer call() {
        try {
            ServiceGetter serv = new ServiceGetter(project.getBasePath() + "/");
            RestJsonMoodleClient RJMC = serv.getRJMC();
            JsonValue value = RJMC.callService("mod_vpl_subrestrictions").get("timeleft");
            if (value.getValueType() == JsonValue.ValueType.NUMBER) {
                return Integer.parseInt(value.toString());
            } else {
                return null;
            }
        } catch (VplConnectionException | MoodleWebServiceException e) {
            return null;
        }
    }
}
