/*
 * Copyright (C) 2022 Joshua Monteiller, Astor Bizard, Christophe Saint-Marcel
 *
 * Caseine Plugin for IntelliJ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Caseine Plugin for IntelliJ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright 2000-2022 JetBrains s.r.o. and other contributors. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.
 * Some functions are under this different licence. Here the list: getModuleType() & getCustomOptionsStep()
 */

package module;

import com.intellij.ide.util.projectWizard.ModuleNameLocationSettings;
import com.intellij.ide.util.projectWizard.ModuleWizardStep;
import com.intellij.ide.util.projectWizard.SettingsStep;
import com.intellij.ide.util.projectWizard.WizardContext;
import com.intellij.openapi.Disposable;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ModifiableRootModel;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import notifications.CaseineNotifier;
import service.PersistentStorage;
import service.ServiceGetter;
import vplwsclient.RestJsonMoodleClient;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.VplConnectionException;

import javax.json.JsonArray;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static service.ServiceGetter.retrieveFiles;

/**
 * This class serves to build the project and the files it will contain after the user validate the new project.
 *
 * @author Joshua Monteiller
 */
public class CaseineModuleBuilder extends com.intellij.ide.util.projectWizard.ModuleBuilder {

    public static final String SPECIAL_FILE_NAME = ".moodlevpl";

    private static final String[] forbiddenCharacters = {"<", ">", ":", "\"", "/", "\\", "|", "?", "*"}; //The list of forbidden characters in the name of a project


    /**
     * Set the VPL project. Add the file needed for the exercise and the special file which stored the VPL ID.
     *
     * @param model
     */
    @Override
    public void setupRootModel(ModifiableRootModel model) {
        Project project = model.getProject();

        PersistentStorage state = PersistentStorage.getInstance();
        //The path to the special file
        String path = project.getBasePath() + "/";

        //Change the state of the boolean, to inform that a new project is in progress of be created
        state.newProjectStatus = true;

        //Create the special file
        createSpecialFile(project);

        // Create .vplignore file
        createVplignoreFile(project);

        //"Download" the required files for the exercise
        ServiceGetter servGet = new ServiceGetter(path);
        JsonArray JArray;
        if (!servGet.haveFailed()) {
            //Depending on if the user have chosen to use an initial project or not
            if (state.initCheckButtonSelected) {
                JArray = servGet.getReqFiles();
            } else {
                JArray = servGet.getFiles();
            }
            try {
                retrieveFiles(JArray, path);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        VirtualFile baseDir = LocalFileSystem.getInstance().refreshAndFindFileByPath(path);
        if (baseDir != null) {
            model.addContentEntry(baseDir);
        }
    }


    /**
     * Creates the special file containing the vpl id
     *
     * @param project The project.
     */
    public static void createSpecialFile(Project project) {
        PersistentStorage state = PersistentStorage.getInstance();
        File f = new File(project.getBasePath() + File.separator + SPECIAL_FILE_NAME);
        try (FileOutputStream fos = new FileOutputStream(f)) {
            fos.write(state.vplID.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Generate a .vplignore file at the root of the project.
     *
     * @param project The project.
     */
    public static void createVplignoreFile(Project project) {
        String vplIgnorePath = project.getBasePath() + File.separator + ".vplignore";
        if (new File(vplIgnorePath).exists()) {
            return;
        }
        try {
            InputStream templateStream = CaseineModuleBuilder.class.getResourceAsStream("/vplignoreTemplate");
            if (templateStream != null) Files.copy(templateStream, Paths.get(vplIgnorePath));
        } catch (IOException e) {
            CaseineNotifier.notifyError(project, "Failure While Creating .vplignore File.", "You can create one manually to solve this.");
        }
    }

    @Override
    public CaseineModuleType getModuleType() {
        return CaseineModuleType.getInstance();
    }


    /**
     * Retrieve the information given by the user and search the exercise name.
     * If found, replace the default name of the project by the name of the exercise
     */
    @Override
    public ModuleWizardStep modifySettingsStep(SettingsStep settingsStep) {
        ModuleNameLocationSettings moduleNameLocationSettings = settingsStep.getModuleNameLocationSettings();
        if (moduleNameLocationSettings != null) {
            String path = moduleNameLocationSettings.getModuleContentRoot();

            //Keep the path and erase from it the last folder that doesn't interest us
            path = path.substring(0, path.lastIndexOf(File.separator) + 1);

            //Get the new name for the project
            String newName = createNameProject(path);
            if (newName.equals("")) {
                super.modifySettingsStep(settingsStep);
            }

            //Change the path and the name of the project
            moduleNameLocationSettings.setModuleContentRoot(path);
            moduleNameLocationSettings.setModuleName(newName);

        }
        return super.modifySettingsStep(settingsStep);
    }

    /**
     * Create a new name for the project thanks to the configuration of the project (with the user token and the VPL ID)
     * The new name will be used to preconfigure the name and path text field when the user will pass to the second step.
     *
     * @param path the actual path of the project
     * @return the new name of the project (the name of the exercise linked to the VPL ID)
     */
    public static String createNameProject(String path) {
        //Retrieve the name of the exercise
        PersistentStorage state = PersistentStorage.getInstance();
        RestJsonMoodleClient RJMC = new RestJsonMoodleClient(state.vplID, state.userToken, state.caseineRadioButtonSelected ? state.caseineURL : state.customURL);
        String name;
        try {
            name = RJMC.callService("mod_vpl_info").getString("name");
        } catch (VplConnectionException | MoodleWebServiceException e) {
            return "";
        }

        //Replace all the forbidden characters which can't be in the name of a project
        for (String s : forbiddenCharacters) {
            if (name.contains(s)) {
                name = name.replace(s, "");
            }
        }
        name = name.replace("  ", " ");


        //Erase in the path the previous name
        //path = path.replace(name, "");

        //If the name already exist, try to add a number at the end to differentiate it
        File dir = new File(path + File.separator + name);
        int i = 1;
        String newName = name;
        while (dir.exists()) {
            newName = name + i;
            dir = new File(path + File.separator + newName);
            i++;
        }
        return newName;
    }

    @Override
    public ModuleWizardStep getCustomOptionsStep(WizardContext context, Disposable parentDisposable) {
        return new CaseineModuleWizardStep();
    }

}
