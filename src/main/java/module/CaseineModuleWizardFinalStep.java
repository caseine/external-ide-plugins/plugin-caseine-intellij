/*
 * Copyright (C) 2022 Joshua Monteiller, Astor Bizard, Christophe Saint-Marcel
 *
 * Caseine Plugin for IntelliJ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Caseine Plugin for IntelliJ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package module;

import com.intellij.ide.util.BrowseFilesListener;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import com.intellij.ui.components.JBLabel;
import com.intellij.util.ui.FormBuilder;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * This class is useful for the fifth button, the New Project button. It imitates the behaviour of the usual empty project wizard of IntelliJ.
 * But at the difference that it can be used on all versions of IntelliJ and keeps working when used for other IDEs of JetBrains like PyCharm.
 * Is used only for CaseineWizardDialog and for CaseineWizardDialogAction.
 *
 * @author Joshua Monteiller
 */
public class CaseineModuleWizardFinalStep extends com.intellij.ide.util.projectWizard.ModuleWizardStep {

    private final JPanel MainPanel;
    private final JTextField NameTextField;
    private final JTextField LocationTextField;

    public CaseineModuleWizardFinalStep() {

        NameTextField = new JTextField();
        LocationTextField = new JTextField();
        ActionListener al = new BrowseFilesListener(LocationTextField, "Title", "Hello", BrowseFilesListener.SINGLE_DIRECTORY_DESCRIPTOR);
        TextFieldWithBrowseButton locationBrowseTextField = new TextFieldWithBrowseButton(LocationTextField, al);

        JPanel namePanel = FormBuilder.createFormBuilder()
                .addComponent(new JBLabel("Name of the project:"))
                .addComponent(NameTextField)
                .getPanel();

        JPanel locationPanel = FormBuilder.createFormBuilder()
                .addComponent(new JBLabel("Location of the project:"))
                .addComponent(locationBrowseTextField)
                .getPanel();


        MainPanel = FormBuilder.createFormBuilder()
                .addComponent(namePanel)
                .addComponent(locationPanel)
                .getPanel();
    }

    @Override
    public JComponent getComponent() {
        return MainPanel;
    }

    @Override
    public void updateDataModel() {
    }

    public void setName(String name) {
        NameTextField.setText(name);
    }

    public void setLocation(String location) {
        LocationTextField.setText(location);
    }

    public String getName() {
        return NameTextField.getText();
    }

    public String getLocation() {
        return LocationTextField.getText();
    }
}
