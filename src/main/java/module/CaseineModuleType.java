/*
 * Copyright (C) 2022 Joshua Monteiller, Astor Bizard, Christophe Saint-Marcel
 *
 * Caseine Plugin for IntelliJ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Caseine Plugin for IntelliJ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Copyright 2000-2022 JetBrains s.r.o. and other contributors. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.

package module;

import com.intellij.notification.Notification;
import com.intellij.notification.NotificationAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.module.ModuleTypeManager;
import com.intellij.openapi.project.Project;
import notifications.CaseineNotifier;
import org.jetbrains.annotations.NotNull;
import ui.icons.CaseineIcons;

import javax.swing.*;
import java.io.File;

import static module.CaseineModuleBuilder.SPECIAL_FILE_NAME;
import static module.CaseineModuleBuilder.createVplignoreFile;

/**
 * This class is used to define the type of the project who will be created in the "New Project" wizard
 *
 * @author Joshua Monteiller
 */
public class CaseineModuleType extends com.intellij.openapi.module.ModuleType<CaseineModuleBuilder> {

    /**
     * An important ID which is used in the plugin.xml file to reference this class
     */
    private static final String ID = "CASEINE_TYPE";

    public CaseineModuleType() {
        super(ID);
    }

    public static CaseineModuleType getInstance() {
        return (CaseineModuleType) ModuleTypeManager.getInstance().findByID(ID);
    }

    @NotNull
    @Override
    public CaseineModuleBuilder createModuleBuilder() {
        return new CaseineModuleBuilder();
    }

    @NotNull
    @Override
    public String getName() {
        return "Caseine Project";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Allow to make a new Caseine Project";
    }

    @NotNull
    @Override
    public Icon getNodeIcon(@Deprecated boolean b) {
        return CaseineIcons.Milk;
    }

    /**
     * Return true if the given project is a caseine (or at least vpl) one.
     * This is detected with the presence of the special file (containing vplID) at the root of the project.
     *
     * @param project current project.
     * @return {@code true} if the project is a Caseine project, {@code false} otherwise.
     */
    public static boolean isCaseineProject(@NotNull Project project) {
        File specialFile = new File(project.getBasePath() + File.separator + SPECIAL_FILE_NAME);
        if (specialFile.exists()) {
            // If there is no .vplignore file, warn the user about potential threat.
            if (!alreadyWarned && !new File(project.getBasePath() + File.separator + ".vplignore").exists()) {
                alreadyWarned = true;
                CaseineNotifier.notifyWarning(project, "No .vplignore file found", "Pull and reset actions may delete files that are not stored on the vpl.",
                        new NotificationAction[]{
                                new NotificationAction("Create .vplignore") {
                                    @Override
                                    public void actionPerformed(@NotNull AnActionEvent e, @NotNull Notification notification) {
                                        createVplignoreFile(project);
                                    }
                                }
                        });
            }
            return true;
        }
        return false;
    }

    private static boolean alreadyWarned = false;
}
