/*
 * Copyright (C) 2022 Joshua Monteiller, Astor Bizard, Christophe Saint-Marcel
 *
 * Caseine Plugin for IntelliJ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Caseine Plugin for IntelliJ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package module;

import service.PersistentStorage;
import vplwsclient.RestJsonMoodleClient;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.VplConnectionException;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static service.PersistentStorage.DEFAULT_NUMBER;

/**
 * This class represents the "New Project" window for the Casein plugin.
 * It allows to make a new project with the user's token and the ID of the exercise.
 * Used a .form file to create the UI.
 *
 * @author Joshua Monteiller
 */
public class CaseineModuleWizardStep extends com.intellij.ide.util.projectWizard.ModuleWizardStep {

    @SuppressWarnings("unused")
    private JPanel MainPanel;
    @SuppressWarnings("unused")
    private JButton tokenButton;
    @SuppressWarnings("unused")
    private JCheckBox initCheck;
    @SuppressWarnings("unused")
    private JPasswordField tokenPassword;
    @SuppressWarnings("unused")
    private JTextField VPL_IDTextField;
    @SuppressWarnings("unused")
    private JTextPane initialProjectTextPane;
    @SuppressWarnings("unused")
    private JTextPane tokenTextPane;
    @SuppressWarnings("unused")
    private JTextPane VPL_IDTextPane;
    @SuppressWarnings("unused")
    private JPanel InitialPanel;
    @SuppressWarnings("unused")
    private JTextPane errorTextField;
    @SuppressWarnings("unused")
    private JLabel errorIcon;
    @SuppressWarnings("unused")
    private JScrollPane errorTextScrollPane;


    public CaseineModuleWizardStep() {
    }

    @Override
    public JComponent getComponent() {
        //Retrieve the token of the user in the storage
        PersistentStorage state = PersistentStorage.getInstance();
        String token = state.userToken;
        state.vplID = DEFAULT_NUMBER;

        //If the token is not given, the user can write. If not, we set the token in the password field, but it can't be modified unless the user use the button
        if (token.equals("")) {
            tokenPassword.setEnabled(true);
        } else {
            tokenPassword.setText(state.userToken);
            tokenPassword.setEnabled(false);
        }

        //If the edit button is clicked once, it allows the user to edit the token. If clicked a second time, the token is saved in the storage and the password field is disabled
        tokenButton.addActionListener(e -> tokenPassword.setEnabled(!tokenPassword.isEnabled()));

        //Set the state of the button to false at the beginning
        state.initCheckButtonSelected = false;
        initCheck.addActionListener(e -> state.initCheckButtonSelected = initCheck.isSelected());

        //A KeyListener for when the user typed, the VPL ID is saved
        VPL_IDTextField.addKeyListener(new KeyListener() {
            public void keyPressed(KeyEvent keyEvent) {
            }

            public void keyReleased(KeyEvent keyEvent) {
                state.vplID = VPL_IDTextField.getText();
                setTextErrorTextField();
            }

            public void keyTyped(KeyEvent keyEvent) {
            }
        });

        //A KeyListener for when the user typed, the token is saved
        tokenPassword.addKeyListener(new KeyListener() {
            public void keyPressed(KeyEvent keyEvent) {
            }

            public void keyReleased(KeyEvent keyEvent) {
                StringBuilder pass = new StringBuilder();
                char[] word = tokenPassword.getPassword();
                for (char c : word) {
                    pass.append(c);
                }
                state.userToken = pass.toString();
                setTextErrorTextField();
            }

            public void keyTyped(KeyEvent keyEvent) {
            }
        });

        //Configure the error message that appears when the user typed a wrong value for the token or the ID
        errorIcon.setVisible(false);
        errorTextScrollPane.setVisible(false);
        errorTextScrollPane.setBorder(BorderFactory.createEmptyBorder());
        errorTextField.setText("You do not have the permissions to access this VPL.\n" +
                "If you should have access to it, please check the VPL ID and your token, or call the platform administrator.");

        return MainPanel;
    }

    @Override
    public void updateDataModel() {
    }

    /**
     * Check if given parameters for vpl are valid and if not display error indications.
     */
    public void setTextErrorTextField() {
        errorIcon.setVisible(false);
        errorTextScrollPane.setVisible(false);
        //errorTextField.setText("");

        PersistentStorage state = PersistentStorage.getInstance();
        RestJsonMoodleClient RJMC = new RestJsonMoodleClient(state.vplID, state.userToken, state.caseineRadioButtonSelected ? state.caseineURL : state.customURL);
        try {
            RJMC.callService("mod_vpl_info").getString("name");
        } catch (VplConnectionException | MoodleWebServiceException e) {
            if (!state.vplID.equals("") && !state.userToken.equals("")) {
                errorIcon.setVisible(true);
                errorTextScrollPane.setVisible(true);

            }
        }
    }


}
