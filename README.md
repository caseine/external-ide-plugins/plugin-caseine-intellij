# Caseine VPL

The VPL extension let you download, upload and evaluate the files required by a Moodle 'Virtual Programming Lab' (VPL) activity from the environment of IntelliJ. Of course, you can also edit and run locally these files and take advantage of IntelliJ for these tasks.

## Features

- simple setup using VPL id
- upload / download tasks managed by a simple button click
- activity tree view for remote evaluation result

This plugin is made specially for Caseine, but you can use it for other VPLs. You need to modify an option in the plugin Settings and make sure that the VPL you want to use have its web services online.

## Release Notes

The latest release is still a prototype that has not yet been tested at scale.

## Documentation

The user documentation of this plugin can be found on [moodle.caseine.org](https://moodle.caseine.org/mod/page/view.php?id=48758).
For further information, please refer to the wiki.

## Overview

For details about IntelliJ plugin development, you can refer to [the JetBrains documentation](https://plugins.jetbrains.com/docs/intellij/developing-plugins.html).\
The plugin is based on Gradle with Kotlin.

Here is a quick description of the different packages in `src/main/java`:

```text
src/main/java
├── action          Code for buttons and new project wizard
├── clock           Code of the timer in status bar
├── comments        Handle comments on specific portions of the code
├── evaluation      Code of the evaluation counter in the status bar
├── module          Creation of new projects and wizard steps
├── notifications   Util class with method to display notifications
├── service         Code for persistent data and interaction with the web service
├── settings        Code for plugin settings
├── sideWindow      The side window contains vpl description and evaluation results
└── ui
    └── icons       Icons management
